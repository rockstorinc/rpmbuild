Name: rockstor
Version: 3.9.2
Release: 48
Summary: RockStor -- Store Smartly
Group: RockStor

%define source %{name}-%{version}

Vendor: RockStor, Inc.
Packager: RockStor, Inc. <support@rockstor.com>
License: GPL
AutoReqProv: no
Source: %{source}.tgz
Prefix: /opt
BuildRoot: /tmp/%{name}
Requires: nginx
Requires: python
Requires: samba
Requires: samba-client
Requires: ypbind
Requires: rpcbind
Requires: ntp
Requires: chrony
Requires: systemtap-runtime
Requires: btrfs-progs
Requires: firewalld
Requires: yum-plugin-changelog
Requires: at
Requires: postgresql
Requires: postgresql-server
Requires: rsync
Requires: nfs-utils
Requires: kernel-ml
Requires: avahi
Requires: netatalk
Requires: smartmontools
Requires: sos
Requires: hdparm
# for e-mail alert setup
Requires: postfix
Requires: cyrus-sasl-plain
# for auto updates
Requires: yum-cron
# easy editor for troubleshooting etc..
Requires: nano
# useful tools for troubleshooting
Requires: usbutils
Requires: pciutils
Requires: epel-release
Requires: nut
Requires: nut-xml
# for AD integration -- sssd
Requires: realmd
#Requires: sssd
#Requires: oddjob
#Requires: oddjob-mkhomedir
#Requires: adcli
Requires: samba-common
# for AD integration -- winbind
Requires: samba-winbind
Requires: krb5-workstation
Requires: samba-winbind-krb5-locator
# AD integratio contd.. for wbinfo
Requires: samba-winbind-clients
# for Rock-ons
Requires: docker-ce
# for Teaming
Requires: NetworkManager-team
#for SNMP
Requires: net-snmp
#for shellinabox aka terminal from the ui/browser feature.
Requires: shellinabox
#for LUKS
Requires: cryptsetup
#for ??
Requires: python-distro
Requires: yum-changelog

Requires(post): systemd
BuildRequires: systemd

%description
%{summary}

%prep
%setup -n %{source}

%build
rm -rf %{buildroot}
mkdir %{buildroot} %{buildroot}/opt
cp -r $RPM_BUILD_DIR/%{source} %{buildroot}/opt/%{name}
cd %{buildroot}/opt/%{name}
python install.py bootstrap
python install.py buildout:extensions=

python -m compileall -q -f -d /opt/%{name}/eggs eggs \
   > /dev/null 2>&1 || true

rm -rf release-distributions

for egglink in develop-eggs/*.egg-link
do
    sed -i "s;%{buildroot};;" ${egglink}
done

cd /tmp

%clean
rm -rf %{buildroot}
rm -rf $RPM_BUILD_DIR/%{source}

%files
%defattr(-, root, root)
%config(noreplace) /opt/%{name}/etc/nginx/nginx.conf
/opt/%{name}

%ghost /opt/%{name}/eggs/setuptools-7.0-py2.7.egg

%pre
# if [ "$1" = "2" ]; then
#     cp /opt/rockstor/conf/rockstor-pre.service /etc/systemd/system/rockstor-pre.service
#     systemctl enable rockstor-pre.service
#     cp /opt/rockstor/conf/rockstor.service /etc/systemd/system/rockstor.service
#     systemctl enable rockstor.service
#     systemctl daemon-reload
#     echo "systemctl daemon-reload done"
#fi
exit 0

%post
#create /dev/btrfs-control if it doesn't exist already.
if [ ! -c /dev/btrfs-control ]; then
    /bin/mknod /dev/btrfs-control c 10 234
fi
#create the udev rule
cp /opt/rockstor/conf/64-btrfs.rules /etc/udev/rules.d/

mkdir -p /etc/systemd/system-preset
cp /opt/rockstor/conf/90-default.preset /etc/systemd/system-preset/

mkdir -p /opt/rockstor/var/log
mkdir -p /opt/rockstor/src/rockstor/logs
if [ "$1" = "2" ]; then

    cp /opt/rockstor/conf/django-hack /opt/rockstor/bin/django

    for f in `find /opt/rockstor/var/log -type f`;
    do echo "Package upgrade" >> $f; done

fi


cp /opt/rockstor/conf/rockstor-pre.service /etc/systemd/system/rockstor-pre.service
cp /opt/rockstor/conf/rockstor.service /etc/systemd/system/rockstor.service
cp /opt/rockstor/conf/rockstor-bootstrap.service /etc/systemd/system/rockstor-bootstrap.service
systemctl enable rockstor-pre rockstor rockstor-bootstrap.service
systemctl daemon-reload
echo "systemctl daemon-reload done"

%systemd_post rockstor.service

exit 0

%postun
if [ "$1" = "0" ]; then
    systemctl stop rockstor
    systemctl disable rockstor
    #not deleting files -- something to consider later
fi
exit 0

%changelog

* Mon Nov 6 2017 Suman Chakravartula <suman@rockstor.com>
failed to start rockstor hdparm settings #1752 @phillxnet
no drive name on custom smart options page #1756 @phillxnet
mark whole disk LVM members as unusable #1710 @phillxnet
Scheduled scrub throws error #1759 @phillxnet
rogue share.pqgroup db field fails share mounts #1769 @phillxnet
improve mount status reporting capability #1763 @phillxnet
Config backup restore error: exception: isdir() takes exactly 1 argument (0 given) #1765 @phillxnet
useradd error upon config restore #1774 @phillxnet
regression on user create with existing non managed group #1780 @phillxnet
improve scrub status reporting resolution #1786 @phillxnet
SCRUB fails to end / fails to restart after shutdown #1640 @phillxnet
fix portability bug in fs unit tests #1787 @phillxnet
regression in 'force' option - scrub and balance #1790 @phillxnet
Change chosen plugin to bootstrap-select or select2 plugin #1270 @priyaganti
improve rebooted dialog message #1584 @phillxnet
Kernel update #1732 @schakrava
Update Chart.js libs for better ticks & labels sliding #1469 @MFlyer
scheduled scrub blocked by halted, conn-reset, and cancelled states #1800 @phillxnet
scrub task end time incorrect #1802 @phillxnet
share and snapshot api endpoints must be by id and not by name #1807 @schakrava
fix scrub in progress message format regression #1776 @phillxnet
Improve messaging re SMART refresh button #1435 @phillxnet
improve share mount re ro,degraded pool options #1804 @phillxnet
snapshot scheduled task regression re 3.9.1-9 API change #1809 @phillxnet
Unable to create Samba export #1813 @phillxnet
Scrub feedback feature #1243 @phillxnet
scrubs 'show entries' selector ineffectual #1799 @phillxnet
rock-on UI fails to reflect removed container #1795 @schakrava
Sysctl.conf optimizations should go into separate file #1598 @schakrava
failure in share import prep re duplicate name and usage #1828 @phillxnet
share link regression on snapshots page #1829 @phillxnet
[BUGS] Default ulimit setting too low #1656 @schakrava
abstract by-id device path in a function #1839 @KaiRo-at
add share detail links to export tables #1830 @phillxnet
Don't print stack trace for update subscription authorization error #1843 @schakrava

* Sun Jul 2 2017 Suman Chakravartula <suman@rockstor.com>
Support full disk LUKS #550 @phillxnet
Schedule power down/up of the system #735 @MFlyer
Graceful shutdown with cron job #1306 @MFlyer
UI Shares view incorrect sort on size #1673 @MFlyer
Rockstor Translations #1643 @MFlyer
Systemd warns that "rockstor-hdparm.service is marked world-inaccessible" #1493 @schakrava
Importing an unlabeled btrfs RAID5 pool fails #1342 @schakrava
single pool metadata level is single not dup #1409 @phillxnet
Samba Custom configuration UI glitch #1691 @MFlyer
rockstor update doesn't fail if db migration fails #1332 @schakrava
Add Yum capabilities to System updates #1619 @MFlyer
Turn off debug logs in Rockstor built by rpm(prod/testing builds) #1478 @schakrava
update nginx and /etc/issue if mgmt interface config changes #1701 @schakrava
Muting gulp tasks over buildout process #1708 @MFlyer
nbd devices do not support S.M.A.R.T. #1705 @KaiRo-at
fix 'for' attributes on user_create_template labels #1706 @KaiRo-at
create SHA256 certs so browser devtools don't complain all the time #1707 @KaiRo-at
overflow of disk.role db field #1709 @phillxnet
Non integer threshold value in SMART data blocks reporting #1725 @phillxnet
support ro rw degraded and skip_balance mount options #1728 @phillxnet
Support Jumbo frames config in the UI #1044 @schakrava
Extra button adding storage to a Rock-On #1341 @priyaganti
Don't check for Rock-ons metadata when the service is disabled #1286 @schakrava
AFP export doesn't expand when underneath share (subvolume in btrfs) expands #614 @schakrava
insufficient use of btrfs device scan #1547 @phillxnet
Rock-on bad behaviour when starting with no configuration #1579 @priyaganti
add cli initiated config backup #1382 @daniel-illi
change Pool API to use ids instead of names #1741 @schakrava
detached disks used in mount command #1422 @phillxnet
Display client-side error differently #1743 @priyaganti
change disk api to use ids instead of names #1746 @schakrava
propagate user errors properly in user management #1748 @schakrava
minor disk api regression follow up #1750 @phillxnet

* Thu Mar 16 2017 Suman Chakravartula <suman@rockstor.com>
Issue: Non-ASCII password on user creation leads to 'User(...) already exists' #1555 @ansemjo
[Enhancement] data_collector move from Django ORM to CRUD operations (on db writes) #1567 @MFlyer
Dashboard widget resizing is glitchy in Chrome on Mac #1530 @MFlyer
[Bug] Disks widget console errors #1596 @MFlyer
cosmetic issue: create snapshot label appears twice in share details view #1564 @MFlyer
SMART edit icon tooltip text in disks table view not fully visible #1565 @MFlyer
More documentation on services page! #1168 @priyaganti
cleanup dependencies and add build status badge to readme #1604 @schakrava
root and home shares offer delete button #1583 @MFlyer
fs unit tests settings issue #1609 @phillxnet
work around failure of udev to observe btrfs device add #1606 @phillxnet
Flake8 satisfying style improvements and cleanup #1615 @schakrava
Sector Size empty for 512e drives #1590 @phillxnet
Fixing Jenkins Flake8 regressions #1626 @MFlyer
enhance disk role/management subsystem #1494 @phillxnet
balance cancel requested 'unit test' false alarm #1627 @phillxnet
Share usage not reported correctly while pool usage is #1412 @MFlyer
improve state column clarity in network device tables #1633 @phillxnet
keyerror in samba config restore #1585 @MFlyer
"Comment" field not filled when editing Samba share #1647 @MFlyer
inconsistent redirect role validator message and bug #1651 @phillxnet
[Rockstor Devel Feature] Add Gulp file testing #1632 @MFlyer
Rock-on share columns reversed #1581 @schakrava
Remove python downgrade workaround #1587 @schakrava
[Flake8] Unused import over initrock.py script #1663 @MFlyer
Small UI bug while installing rock-ons #1660 @MFlyer
fix replication regression from django 1.8 update #1667 @schakrava
Make software update non-disruptive #1188 @schakrava
Updating Font Awesome to latest 4.7 #1669 @MFlyer

* Tue Dec 13 2016 Suman Chakravartula <suman@rockstor.com>
Improve Dashboard pool usage widget #1426 @sfranzen
Improve pool usage reporting #1460 @sfranzen
Linked to #1379 - writable snapshots #1482 @MFlyer
Show up Rockstor as a Server in AFP Shares #1485 @MFlyer
Ajax-Loader.gif does not dissappear #623 @MFlyer
Increase widget animation timing to grant a better smoothing #1487 @MFlyer
NUT timed shutdown option #982 @phillxnet
improve NUT port config usability #1458 @phillxnet
Fix time comparison failing #1479 @MFlyer
null value in column "details" when parsing SMART error log #1498 @phillxnet
Remove flash websocket files #848 @MFlyer
Glitch in SMART error log UI #1001 @phillxnet
SMART parsing issue shell script helper #1507 @phillxnet
Small Samba regression #1495 @MFlyer
Move from gevent-socketio python-socketio #1503 @MFlyer
Make services configure forms as modals #1278 @MFlyer
remove gateway requirements for manual interface configuration #1520 @tomtom13
Improve Pool delete UX #1195 @gkadillak
use chattr to make parent dirs of mount points immutable #1414 @schakrava
Missing favicon on login page #1535 @MFlyer
failure to submit on modal service dialogs #1537 @MFlyer
support pool compression inline edit in pool details view #1464 @priyaganti
Update Django #1190 @schakrava
Improved handling of spawned functions in socket server #1524 @MFlyer
Samba configuration enhancements #1540 @MFlyer
incorrect space calculation on disk removal #1553 @phillxnet
Wrong Samba AD closing line #1548 @MFlyer
unsaved related object fix #1551 @phillxnet
Resize UI bug #1194 @priyaganti
Fix loading while edit scheduled tasks #1561 @MFlyer
Snapshots & scrubs scheduled tasks not working - double issue #1560 @schakrava
data_collector changes for django >= 1.7 #1556 @MFlyer
add datatables feature to users view #1370 @MFlyer
add datatables feature to pool srub and rebalance tables #1369 @MFlyer
Pool creation succeeds but refresh icon(ajax-loader) still exists #1563 @MFlyer

* Tue Nov 1 2016 Suman Chakravartula <suman@rockstor.com>
Overhaul pagination, sort and search on UI using DataTables. #1138 @priyaganti
Revise internal use and format of device names. #1320 @phillxnet
Support customizing web-ui port. #983 @schakrava
Improvements to password recovery system. #1290 @MFlyer
Remove smb service dependency on rockstor-bootstrap. #1241 @schakrava @phillxnet
add raid56 warning. #1372 @phillxnet
Fix Samba regression from 3.8-14.03 #1385 @phillxnet
Add local/current time on the UI. #1362 @MFlyer
Update postfix config when hostname is changed. #1392 @MFlyer
Edit user page - bad render for username and uid. #1389 @MFlyer
Support Console access from the Web-UI with Shell In a Box. #518 @MFlyer
Allow force removal of Rock-on metadata. #1124 @schakrava
improve nvme compatibility for system disk. #1397 @phillxnet
Fix Services page bottleneck on Active Directory status. #1391 @MFlyer
Improve test e-mail notification. #978 @MFlyer @schakrava
Field validation in e-mail setup. #1340 @MFlyer @schakrava
GMail detects Rockstor as a Less Secure application. #1083 @MFlyer
single to raid1 pool resize not reflected in Web-UI. #1406 @grebnek
Email Alerts page missing dependency. #1410 @MFlyer
Docker journald logging. #1420 @sfranzen
Adjust share usage reporting. #1415 @sfranzen
add samba shadow localtime param. #1252 @MFlyer
Improve Dashboard pool usage widget. #1426 @sfranzen
Fix: argument to docker run should still be -d. #1423 @sfranzen
Fix DataTables error on AFP shares view. #1442 @sfranzen
Bootstrap inline edit with X-editable js library. #1356 @priyaganti
Inline edit - pool compression in Pools view. #1401 @priyaganti
Web-UI initiated balance status not updated during execution. #1405 @phillxnet
improve dashboard disk activity widget for by-id names. #1366 @phillxnet
Allow scheduling of read-only snapshot creation. #1379 @tomtom13 @schakrava
Improve how smb.conf is updated. #1453 @MFlyer
Fix: Dashboard crashes if left open for a long time #998 @MFlyer
add the second knowns fake uuid to exception list. #1461 @schakrava
fs unittests part 1. #1443 @phillxnet
dashboard crashes if opened long - Memory Widget - Part 3. #998 @MFlyer
dashboard crashes if opened long - Cpu Widget - Part 1 Final. #998 @MFlyer
dashboard crashes if opened long - Network Widget - Part 2. #998 @MFlyer
dashboard crashes if opened long - Top Shares Widget - Part 4. #998 @MFlyer
dashboard crashes if opened long - Pool Usage Widget - Part 5. #998 @MFlyer
dashboard crashes if opened long - Storage Metrics Widget - Part 6. #998 @MFlyer
dashboard crashes if opened long - Disks Widget - Part 7. #998 @MFlyer
New progressbars height, tested over 10+ shares. #1476 @MFlyer
support long nutanix device names. #1471 @phillxnet

* Sun Jun 19 2016 Suman Chakravartula <suman@rockstor.com>
Add anacron like feature to task scheduling. #1233 @MFlyer
Add support for policy driven powering down of HDDs from the UI. #885 @phillxnet
Add the feature to browse and download various log files from the UI. #762 @MFlyer
Significantly improve UI templates part 2. #1287 @priyaganti
Significantly improve UI templates part 3. #1304 @priyaganti
Significantly improve UI templates part 4. #1307 @priyaganti
Add different support flows for stable and testing channel users. #1339 @schakrava
Improve Active Directory info popup. #1284 @ScarabMonkey
Improve multiple disk selection during resize. #1196 @priyaganti
Show correct screens in add/remove disks during resize. #811 @priyaganti
Fix power menu alignment. #1192 @priyaganti
Use chardet to properly encode/decode user/group names. #1283 @demount
Add Active Directory rfc2307 support. #1263 @MFlyer
Sort Shares by name in the UI. #973 @maxhq
Fix a regression in scheduled tasks. #1296 @MFlyer
Fix Total capacity widget resize bug. #1225 @MFlyer
Fix deprecated volume removal bug in rock-on update. #1294 @phillxnet
Fix transfer rate column in replication history. #1279 @priyaganti
Improve NTP check in Active Directory service. #1301 @ScarabMonkey
Enhance Rock-on service config UX. #1202 @priyaganti
Properly update mdraid member status. #1214 @phillxnet
Add pagination support to replication tasks. #1305 @priyaganti
Improve snmp config UI. #1240 @schakrava
Humanize replication transfer rate display. #1317 @priyaganti
Fix bug in Pool disk removal wizard. #1325 @phillxnet
Fix a bug in scheduled tasks. #1327 @MFlyer
Add support for nossd mount option. #1313 @priyaganti
Fix regression in network widget. #1302 @MFlyer
Show system users shell info. #1335 @MFlyer
Add the ability of add/remove drives to/from single profile pools. #1337 @bskrtich
Fix a bug in userdel. #1343 @MFlyer
Make Appliance UUID persistent through reinstalls. #1348 @schakrava
Add better error handling to network connection refresh. #1350 @schakrava
Fix handlebar helper in replication. #1352 @priyaganti

* Wed Apr 20 2016 Suman Chakravartula <suman@rockstor.com>
Add Network Teaming and Bonding support. #560 @schakrava @priyaganti
Support user supplied custom S.M.A.R.T parameters. #1079 @phillxnet
Redesign Services page. #796 @priyaganti
Advice user to use nmtui in case of install without network. #1268 @ScarabMonkey
Allow Rock-on metadata update on failed installation. #1259 @schakrava
Add optional smtp authentication to e-mail notification setup. #1228 @MFlyer
Handlebars template improvements in samba UI. #1176 @priyaganti
Add support to sort services by status in the UI. #1201 @MFlyer
Improve storage unmount logic. #1242 @schakrava
Make adding storage to Rock-ons more intuitive. #1178 @priyaganti
Improve tooltip display. #1198 @priyaganti
Make favicon access secure. #1055 @priyaganti
Fix compression UI bug. #1245 @priyaganti @MFlyer
Improve Rock-on install state transition logic. #1216 @schakrava
Mount by-label consistently as first preference. #1181 @schakrava
Fix Samba UI pagination. #1224 @schakrava
Improve scheduled Snapshot management. #1227 @MFlyer
Add more frequency choices for scheduled tasks. #1226 @MFlyer
Automatically map /etc/localtime to Rock-ons. #809 @schakrava
Fix dashboard by locking backend library versions. #1215 @schakrava
Improve S.M.A.R.T self test log parsing. #1207 @phillxnet
Improve S.M.A.R.T behaviour on root drive. #1206 @phillxnet
Fix snapshot name prefix bug. #1186 @schakrava

* Thu Feb 25 2016 Suman Chakravartula <suman@rockstor.com>
Added support for hostname configuration. #896 @Mchakravartula
Improved S.M.A.R.T support for more types drives. #1107 @phillxnet
Improved tooltip design. #1110 @Mchakravartula
Redesigned Pool creation UI to handle large number of drives better. #693 @priyaganti
Improved Share size reporting. #669 @schakrava
Added support for dynamic root Pool name retrieval. #921 @schakrava
Made rockstor-pre service more robust. #1128 @schakrava
Fixed bug in updating nginx on ip changes. #1101 @schakrava
Improved disk information handling of system disk(s). #1116 @phillxnet
Improved AD integration via winbind. #1024 @schakrava
Improved Share deletion UX and warnings. #979 @priyaganti
Improved bulk Snapshot deletion UX. #988 @priyaganti
Fixed a bug in schedule task history display. #1129 @Mchakravartula
Improved bios raid handling on system disk. #1151 @phillxnet
Fixed version display bug in the UI. #1119 @schakrava
Improved Rock-On app profile updates. #1131 @schakrava
Improved support for mdraid root disk setup. #1164 @phillxnet
Change font and color of banner elements. #1165 @gkadillak
Improved Rock-On restart policy. #1132 @schakrava
Fixed a bug in group creation. #1161 @schakrava
Fixed a bug in AD join. #1122 @schakrava
Added support for force removal of Shares. #1125 @schakrava
Added support for custom port in e-mail alerts setup. #837 @schakrava
Fixed a UI bug in Rock-On restart. #1175 @nicolaslt
Fixed a templating bug in Samba exports UI. #1176 @schakrava
Improved design of Services view. #796 @priyaganti @schakrava

* Fri Jan 22 2016 Suman Chakravartula <suman@rockstor.com>
Significantly improved Rock-Ons functionality. #858 @schakrava
Overhauled and optimized frontend with better temlating using Handlebars. #1019 @priyaganti @schakrava
Improved disk scan to handle duplicate names and offline disks better. #937 @phillxnet
Updated django-auth-toolkit and improved the Access Key functionality. #1017 @schakrava
Fixed a bug in S.M.A.R.T monitoring functionality. #1060 @phillxnet
Simplified Rock-on app profile management. #842 @schakrava
Enhanced custom config implementation in Rock-on install wizard. #918 @schakrava
Added support for bigger SSL certs of size up to 12K. #1067 @schakrava
Enhanced state refresh for Shares and Pools when underlying disks drop. #930 @schakrava
Added better support for drive name changes. #897 @schakrava
Fixed admin host related bug in NFS management. #959 @schakrava
Added better handling for md block drives. #1063 @phillxnet
Made Rock-ons framework more robust and simpler. #858 @schakrava
Added S.M.A.R.T support for MSA70 enclosures. #997 @phillxnet
Fixed minor regression in fake serial ui logic. #1086 @phillxnet
Added support for retaining only last 5 kernels. #1068 @schakrava
Made Access Key API a bit more robust. #1080 @Mchakravartula
Fixed a regression with raw S.M.A.R.T error log display. #1084 @phillxnet
Fixed a UI bug in schedule task display. #1058 @Mchakravartula
Improved config ownership management of NUT. #1073 @phillxnet
Fixed a regression in NUT service configuration in the UI. #1094 @phillxnet
Fixed a Share ACL display bug in the UI. #1100 @phillxnet
Improved README. #1104 @Mazo
Improved messaging for S.M.A.R.T self tests on the UI. #1097 @phillxnet

* Fri Dec 11 2015 Suman Chakravartula <suman@rockstor.com>
Rolled out a redesigned Rockstor-Rockstor Share replication feature. #886 @schakrava
Improved functional test coverage. part 1 #992 @Mchakravartula
Improved alerts on the UI #989 @priyaganti
CSS cleanup in the UI #1003 @grogi
Fixed advanced-nfs exports bug. #991 @schakrava
Improved functional test coverage. part 2. #1008 @Mchakravartula
Redesigned switches on the UI. #1013 @priyaganti
Improved functional test coverage. part 3. #1014 @Mchakravartula
Fixed SFTP service toggle bug. #1027 @phillxnet
Fixed AD service status bug. #1029 @phillxnet
Improved rockstor-bootstrap service. #1026 @schakrava
More switch redesign updates on the UI. #1021 @priyaganti
Fixed a bug in network config. #1039 @schakrava

* Wed Oct 28 2015 Suman Chakravartula <suman@rockstor.com>
Improved service orchestration by leveraging Systemd more. #904 @schakrava
Fixed Web-UI to dynamically refresh management interface IP. #917 @schakrava
Fixed a Web-UI issue with network interface management. #915 @schakrava
Clarify password reset instructions. #890 @phillxnet
Refresh Pool state automatically after delete. #859 @schakrava
Improved logic to update /etc/issue with Web-UI link. #924 @phillxnet
Improved certificate labeling on the Web-UI. 938 @phillxnet
Fixed and improved Active Directory integration support. #860 @schakrava
Simplified Reboot/Shutdown functionality. #943 @phillxnet
Sort services alphabetically by default. #907 @schakrava
Improve test coverage for Snapshot functionality. #945 @Mchakravartula
Clean up Web-UI for OS Pool. #926 @Mchakravartula
Improve test coverage for Network interface management. #954 @Mchakravartula
Fixed broken S.M.A.R.T data collection for some HDDs. #657 @phillxnet
Fixed Web-UI bug that prevented Cloning writable Snapshots. #939 @schakrava
Fixed a small regression in /etc/issue update. #961 @phillxnet
Fixed the submit button in Rock-ons install wizard. #975 maxhq
Improved e-mail notifications by properly setting send address. #970 @phillxnet
Improved overall functional test coverage. #967 @Mchakravartula
Removed qgroup rescan work around. #950 @schakrava

* Sat Oct 3 2015 Suman Chakravartula <suman@rockstor.com>
Added support for UPS. #595 @phillxnet
Added Shadow Copy support for Windows Samba clients. #715 @schakrava @priyaganti
Improved Network management #799 @schakrava
Improved User management functional test coverage #856 @Mchakravartula
Improved NFS functional test coverage #876 @Mchakravartula @schakrava
Improved AFP functional test coverage #877 @Mchakravartula
Improved SFTP functional test coverage #878 @Mchakravartula
Made development side of supervisor processes consistent with production. #852 @phillxnet
Improved UI for mobile #869 @snamstorm
Made forms on the UI better and consistent. #891 @priyaganti
Added Stable and Testing updates channels. #899 @schakrava

* Tue Sep 1 2015 Suman Chakravartula <suman@rockstor.com>
Added more functional tests. #828 @Mchakravartula
Improved styling of the login page. #815 @snamstorm
Made ssh access stricter and fixed a sftp related bug. #838 @schakrava
Improved display of contributions during update. #845 @priyaganti
Improved top menu bar in the UI. #832 @snamstorm
Added support to use system timezone. #712 @schakrava
Improved sidebar menu in the UI. #840 @snamstorm
Cleaned up Share detail page in the UI. #814 @Mchakravartula
Add quota support for imported pools and shares. #687 @schakrava

* Mon Aug 24 2015 Suman Chakravartula <suman@rockstor.com>
Enhanced the auto software update process. #695 @schakrava
Improved disk serial number identification logic. #757 @phillxnet
Added support for Rock-on installation using Firefox browser. #744 @priyaganti
Made Rock-on service state persistent after reboot. #721 @schakrava @phillxnet
Fixed buggy NFS export management #713 @schakrava
Improved functional test coverage #733 @Mchakravartula
Fixed S.M.A.R.T service configuration management. #741 @schakrava @phillxnet
Fixed NTP service for it's state to be persistent after reboot. #768 @schakrava
Improved e-mail notification setup. #793 @phillxnet
Improved wording #792 @phillxnet
Fixed a minor UI bug to refresh properly after a disk wipe. #635 @schakrava
Improved tooltips #802 @priyaganti

* Fri Aug 14 2015 Suman Chakravartula <suman@rockstor.com>
Reworked the data-collector service and removed dashboard polling. Both server and client side performance is significantly improved and all unnecessary IO is eliminated. #763 @gkadillak @schakrava
Fixed buggy behavior with display and deletion of Clones. #727 @schakrava
Added support for e-mail setup for receiving notifications and alerts. #633 @Mchakravartula @schakrava
Fixed broken UI during Share rollback. #774 @schakrava
Fixed Samba export field display on edit. #767 @Mchakravartula
Added support to set Samba workgroup from the UI. #678 @schakrava
Enhanced Share ownership edit form to display sorted users and correct pre-select values. #775 @Mchakravartula
Reworked Pool resize logic to be simpler and more robust. #580 @schakrava

* Fri Jul 31 2015 Suman Chakravartula <suman@rockstor.com>
Added support for config(system state like users, exports etc..) backup and restoration. #392 @schakrava @gkadillak
Fixed task scheduling. #728 @gkadillak @schakrava
Fixed units drop down in Share resize form #745 @Mchakravartula
Fixed advanced nfs exports feature #754 @schakrava
Fixed wrong kernel warning message #746 @gkadillak
Fixed scrub status parsing #752 @schakrava

* Wed Jul 15 2015 Suman Chakravartula <suman@rockstor.com>
Optimize Rockstor for usb flash drive and ssd deployments. #724 @schakrava @phillxnet
Improve Rock-on logging with syslog. #723 @schakrava
Revive and enhance websocket framework and the WebUI. #706 @gkadillak @phillxnet
Make service state changes persist across reboot. #660 @schakrava @phillxnet`

* Mon Jul 6 2015 Suman Chakravartula <suman@rockstor.com>
Significantly improved the Rock-ons framework. #697 @schakrava @gkadillak @phillxnet
Added a new exciting Rock-on: OwnCloud. #697 Thanks @schakrava @gkadillak @phillxnet
Overhauled look and feel of the UI by upgrading Bootstrap. #670 @gkadillak
Dynamically update raid level changes from the system. #650 @schakrava
Improved test coverage for Samba API. #679 @Mchakravartula
Improved test coverage for User API. #688 @Mchakravartula

* Wed Jun 17 2015 Suman Chakravartula <suman@rockstor.com>
Added support to Auto import BTRFS data from previous install or a different Rockstor system. Issue #534
Added more functional tests and improved test coverage. Issue #676
Enhanced disk scan logic to better support KVM. Issue #673

* Sun Jun 7 2015 Suman Chakravartula <suman@rockstor.com>
Improved Rock-ons code over all and added Syncthing, Transmission and BTSync. Issue #665
Added functional tests to improve coverage by about 10%. Issue #648
Fixed samba password reset issue. Issue #617

* Wed May 20 2015 Suman Chakravartula <suman@rockstor.com>
Improved user management to be consistent with system state. Issue #613
Improved HDD wipe functionality for non-btrfs filesystems. Issue #572
Fixed misc bugs related to S.M.A.R.T support.

* Mon May 18 2015 Suman Chakravartula <suman@rockstor.com>
Added S.M.A.R.T support for monitoring HDDs. You can now proactively monitor the health of your Hard Drives. Issue #498
Improved the core API by upgraded to Django Rest Framework(DRF) 3.1. Issue #637
Improved automated testing with new functional tests for Pool related featureset. Issue #639

* Wed Apr 29 2015 Suman Chakravartula <suman@rockstor.com>
Fixed a minor bug in replication code that was causing problems if appliance id starts with a number. Issue #616
Optimized service monitor to do minimal IO. The new code is significantly better and especially helpful to those users running Rockstor from Flash. Issue #585
Added a new Snapshots screen at the top level in the Storage Tab. All snapshots on the system can now be managed from one screen. Issue #573

* Sat Apr 18 2015 Suman Chakravartula <suman@rockstor.com>
Enhanced shutdown/reboot such that web-ui refresh doesn't repeat itself. Issue #612
Enhanced web-ui validation so users are clearly prohibited from choosing share, snapshot etc.. names with spaces in them. Issue #622

* Tue Apr 14 2015 Suman Chakravartula <suman@rockstor.com>
New feature -- Rockons. Added framework support with limited release of Rockons. Issue #542
Added support for custom TLS certificate. Users can add a custom certificate for the web-ui. Issue #570

* Sat Mar 7 2015 Suman Chakravartula <suman@rockstor.com>
Added support for automatic scheduled updates. Once enabled, Rockstor checks for updates once a day. Issue #604
Added wizard support to pool resize to properly guide the user. Issue #577

* Tue Feb 17 2015 Suman Chakravartula <suman@rockstor.com>
Added support for Share creation on the root disk. Now you get a bonus pool called rockstor_rockstor. Consume it responsibly. Issue#600
Fixed a bug that forced create mask on samba exports. Now you can override the default(744) via custom configuration. Issue#587
Added a web-ui goodie. The logged in user will be displayed on the web-ui. Issue#576

* Mon Feb 9 2015 Suman Chakravartula <suman@rockstor.com>
Enhanced Rockstor to Rockstor Share replication. This is a significant feature that can be used to keep your Shares automatically synced between two Rockstor appliances. Issue#574
Fixed a bug in Raid5/6 pool creation. Raid5/6 is still actively being tested, but this bug caused error on creation. Issue#579
Enhanced oauth token generation for api access and made it more efficient. Issue#590

* Tue Jan 20 2015 Suman Chakravartula <suman@rockstor.com>
Added Pool raid level migration. Disks can be added and raid level can be changed. Disks can also be removed from Pools. Issue#517
Added support for providing custom Samba configuration during Share export. Issue#555
Added support to kick of a Pool balance process to redistribute Pool data among it's disks. Issue #510
Fix a bug that prevented Samba exports to be deleted. Issue #566

* Wed Jan 07 2015 Suman Chakravartula <suman@rockstor.com>
Updated kernel to 3.18.1 and fixed a minor bug related to it. Issue #557
Enhanced compression support. Issue #559
Improved snapshot scheduled task management. Issue #475

* Wed Dec 24 2014 Suman Chakravartula <suman@rockstor.com>
Added support for compression. It can be enabled both at the Pool level or at individual Share level. Issue #468
Redesigned snapshots to manage them more cleanly. Issue #536
Simplified SMB and AFP share discovery. Issue #536
Enhanced scheduled task functionality. You can set a number of Snapshots to retain and older snapshots will be deleted according to this policy. Issue #513

* Wed Dec 10 2014 Suman Chakravartula <suman@rockstor.com>
Enhanced support for multiple network interfaces. Issue #521
Fixed Pool scrub bug and improved it overall. Issue #516
Better handling of Disks without serial numbers. Issue #545
Fixed bug resulting from disk name changes triggered by replacement etc.. Issue #546

* Sun Nov 23 2014 Suman Chakravartula <suman@rockstor.com>
Fixed snapshot clone delete bug. Issue #520
Enhanced ui input validation. Issue #541
Enhanced pool creation UX. Issue #434
Misc UI improvements. Issue #537 and #538

* Sat Nov 15 2014 Suman Chakravartula <suman@rockstor.com>
Added SNMP support. Issue #469
Fixed minor snapshot rollback bug. Issue #511
Enhanced user/group management. Issue #401
Enhanced Memory widget in the dashboard. Issue #371

* Thu Nov 6 2014 Suman Chakravartula <suman@rockstor.com>
Fixed Samba guest access. Issue #515
Enhanced Pool resize functionality. Issue #299
Added a few minor UI enhancements. Issue #522

* Sat Oct 25 2014 Suman Chakravartula <suman@rockstor.com>
Added AFP support. Shares can be exported via AFP protocol and Rockstor can be used as a Time Machine server. Issue #323
Added a new widget to show overall capacity and usage information of the system. Issue #429
Added support to identify drives physically and replace them easily if necessary. Issue #499
Fixed a SFTP related bug that could cause sshd service to terminate. Issue #505

* Fri Oct 24 2014 Suman Chakravartula <suman@rockstor.com>
Added AFP support. Shares can be exported via AFP protocol and Rockstor can be used as a Time Machine server. Issue #323
Added a new widget to show overall capacity and usage information of the system. Issue #429
Added support to identify drives physically and replace them easily if necessary. Issue #499
Fixed a SFTP related bug that could cause sshd service to terminate. Issue #505

* Wed Oct 15 2014 Suman Chakravartula <suman@rockstor.com>
Added shutdown/reboot feature in the web-ui. Issue #466
Fixed snapshot rollback bug. Issue #492
Enhanced nfs service display and switch on the web-ui. Issue #483

* Thu Oct 9 2014 Suman Chakravartula <suman@rockstor.com>
Enabled edit functionality for samba exports and enhanced the interface. Issue #481 and #467
Fixed a bug and made service monitoring more stable. Issue #476

* Wed Oct 1 2014 Suman Chakravartula <suman@rockstor.com>
!!!!SPECIAL NOTE!!! This particular update may cause the web-ui to wait for ever. Manually refresh your browser after few minutes.
Enhanced Share create form and Pool usage display on the web-ui. Issue #418
Fixed TLS cert generation so it happens on user's Rockstor box and self-signed by the user. Issue #485

* Sat Sep 27 2014 Suman Chakravartula <suman@rockstor.com>
Fixed device scan issue that created issues after reboot. Issue #465
Misc ui enhancements. Issue #452, #450, #480

* Thu Sep 18 2014 Suman Chakravartula <suman@rockstor.com>
Fixed minor SFTP related bugs. Issue #394
Added ssh pub key as an optional input during user creation. Issue #463

* Mon Sep 1 2014 Suman Chakravartula <suman@rockstor.com>
Fresh new Rockstor 3.0 rpm that's installed along with the OS.
